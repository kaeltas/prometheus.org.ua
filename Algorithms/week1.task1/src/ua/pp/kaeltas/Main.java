package ua.pp.kaeltas;

import java.math.BigInteger;

public class Main {

    public static void main(String[] args) {

        /*check = 8;
        counter = 0;
        System.out.println(karacuba(49823261L, 44269423L));
        System.out.println(counter);

        check = 7;
        counter = 0;
        System.out.println(karacuba(49823261L, 44269423L));
        System.out.println(counter);

        check = 5;
        counter = 0;
        System.out.println(karacuba(54761293L, 65394884L));
        System.out.println(counter);
        */

        /*check = 18;
        counter = 0;
        System.out.println(karacuba(9313685456934674L, 7658898761837539L));
        System.out.println(counter);
        check = 9;
        counter = 0;
        System.out.println(karacuba(9313685456934674L, 7658898761837539L));
        System.out.println(counter);
        check = 8;
        counter = 0;
        System.out.println(karacuba(9313685456934674L, 7658898761837539L));
        System.out.println(counter);*/

        /*check = 14;
        counter = 0;
        System.out.println(karacuba(3957322621234423L, 7748313756335578L));
        System.out.println(counter);
        check = 9;
        counter = 0;
        System.out.println(karacuba(3957322621234423L, 7748313756335578L));
        System.out.println(counter);
        check = 8;
        counter = 0;
        System.out.println(karacuba(3957322621234423L, 7748313756335578L));
        System.out.println(counter);*/

        /*check = 14;
        counter = 0;
        System.out.println(karacubaBI(new BigInteger("3957322621234423"), new BigInteger("7748313756335578")));
        System.out.println(counter);
        check = 9;
        counter = 0;
        System.out.println(karacubaBI(new BigInteger("3957322621234423"), new BigInteger("7748313756335578")));
        System.out.println(counter);
        check = 8;
        counter = 0;
        System.out.println(karacubaBI(new BigInteger("3957322621234423"), new BigInteger("7748313756335578")));
        System.out.println(counter);*/

        /*check = 64;
        counter = 0;
        System.out.println(karacubaBI(new BigInteger("8711129198194917883527844183686122989894424943636426448417394566"),
                new BigInteger("2924825637132661199799711722273977411715641477832758942277358764")));
        System.out.println(counter);
        check = 60;
        counter = 0;
        System.out.println(karacubaBI(new BigInteger("8711129198194917883527844183686122989894424943636426448417394566"),
                new BigInteger("2924825637132661199799711722273977411715641477832758942277358764")));
        System.out.println(counter);
        check = 58;
        counter = 0;
        System.out.println(karacubaBI(new BigInteger("8711129198194917883527844183686122989894424943636426448417394566"),
                new BigInteger("2924825637132661199799711722273977411715641477832758942277358764")));
        System.out.println(counter);*/


        check = 105;
        counter = 0;
        System.out.println(karacubaBI(new BigInteger("1685287499328328297814655639278583667919355849391453456921116729"),
                new BigInteger("7114192848577754587969744626558571536728983167954552999895348492")));
        System.out.println(counter);
        check = 72;
        counter = 0;
        System.out.println(karacubaBI(new BigInteger("1685287499328328297814655639278583667919355849391453456921116729"),
                new BigInteger("7114192848577754587969744626558571536728983167954552999895348492")));
        System.out.println(counter);
        check = 12;
        counter = 0;
        System.out.println(karacubaBI(new BigInteger("1685287499328328297814655639278583667919355849391453456921116729"),
                new BigInteger("7114192848577754587969744626558571536728983167954552999895348492")));
        System.out.println(counter);


/*
        System.out.println("size=");
        System.out.println(getNumberSize(5L));
        System.out.println(getNumberSize(0L));
        System.out.println(getNumberSize(12L));
        System.out.println(getNumberSize(123L));
        System.out.println(getNumberSize(1234L));
        System.out.println(getNumberSize(54761293L));*/

    }


    public static Long pow(Integer x, Integer y) {
        Long result = x.longValue();
        for (int i = 1; i < y; i++) {
            result *= x;
        }
        return result;
    }

    public static Integer getNumberSize(Long x) {
        if (x == 0) return 1;

        Integer result = 0;
        for (long i = x; i > 0; i /= 10) {
            result++;
        }
        return result;

    }


   /*
    public static Integer getNumberSize(Long x) {
        if (x == 0) return 1;

        Integer result = 0;
        for (long i = x; i > 0; i /= 10) {
            result++;
        }
        return result;

    }*/


    static long counter = 0;
    static long check = 8;

    public static Long karacuba(Long x, Long y) {
        Integer sizeX = getNumberSize(x);
        Integer sizeY = getNumberSize(y);

        if (sizeX == sizeY && sizeX == 1) {
            return x*y;
        }

        Integer size;

        if (sizeX > sizeY) {
            size = (sizeX%2==0)?sizeX:sizeX+1;
        } else if (sizeX < sizeY) {
            size = (sizeY%2==0)?sizeY:sizeY+1;
        } else {
            size = (sizeX%2==0)?sizeX:sizeX+1;;
        }

        //System.out.println("-->"+size);

        Long c1 = pow(10, size);
        Long c2 = pow(10, size/2);

        Long a = x/c2.intValue();
        Long b = x%c2.intValue();
        Long c = y/c2.intValue();
        Long d = y%c2.intValue();

        Long ac = karacuba(a, c);
        Long bd = karacuba(b, d);

        //System.out.println("ssssum="+((a+b)*(c+d)));
        Long sum = karacuba((a+b),(c+d))-ac-bd;

        if (sum == check) {
            counter++;
        }

        //System.out.println("ac="+ac);
        //System.out.println("bd="+bd);


        Long xy = c1*ac+c2*(sum) + bd;

        return xy;
    }

    public static BigInteger karacubaBI(BigInteger x, BigInteger y) {
        Integer sizeX = x.toString().length();
        Integer sizeY = y.toString().length();

        if (sizeX == sizeY && sizeX == 1) {
            return x.multiply(y);
        }

        Integer size;

        if (sizeX > sizeY) {
            size = (sizeX%2==0)?sizeX:sizeX+1;
        } else if (sizeX < sizeY) {
            size = (sizeY%2==0)?sizeY:sizeY+1;
        } else {
            size = (sizeX%2==0)?sizeX:sizeX+1;;
        }

        //System.out.println("-->"+size);

        BigInteger c1 = new BigInteger("10").pow(size);
        BigInteger c2 = new BigInteger("10").pow(size/2);

        BigInteger a = x.divide(c2);
        BigInteger b = x.mod(c2);
        BigInteger c = y.divide(c2);
        BigInteger d = y.mod(c2);

        BigInteger ac = karacubaBI(a, c);
        BigInteger bd = karacubaBI(b, d);

        //System.out.println("ssssum="+((a+b)*(c+d)));
        BigInteger sum = karacubaBI(a.add(b),c.add(d)).add(ac.negate()).add(bd.negate());

        if (sum.longValue() == check) {
            counter++;
        }

        //System.out.println("ac="+ac);
        //System.out.println("bd="+bd);


        BigInteger xy = c1.multiply(ac).add(c2.multiply(sum)).add(bd);

        return xy;
    }

}
