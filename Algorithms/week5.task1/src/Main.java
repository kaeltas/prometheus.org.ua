import java.io.*;
import java.lang.reflect.Field;

/**
 * Created by kaeltas on 06.03.15.
 */
public class Main {

    public static void main(String[] args) {

        MinPQ<Integer> minPQ = new MinPQ<Integer>();
        MaxPQ<Integer> maxPQ = new MaxPQ<Integer>();

        File inputFile = new File("test-data/input_16_10000.txt");
        try {
            BufferedReader br = new BufferedReader(new FileReader(inputFile));

            String countStr = br.readLine();
            Integer count = Integer.valueOf(countStr);

            for (int i = 0; i < count; i++) {
                String valueStr = br.readLine();
                Integer value = Integer.valueOf(valueStr);

                if (!maxPQ.isEmpty() && value < maxPQ.max()) {
                    maxPQ.insert(value);
                } else {
                    minPQ.insert(value);
                }

                if (minPQ.size() > maxPQ.size()+1) {
                    maxPQ.insert(minPQ.delMin());
                } else if (maxPQ.size() > minPQ.size()+1) {
                    minPQ.insert(maxPQ.delMax());
                }

                //result for iteration #2015
                if (i+1 == 2015) {
                    //reflection
                    Class clazzMaxPQ = maxPQ.getClass();
                    Field fieldPqMaxPQ = clazzMaxPQ.getDeclaredField("pq");
                    fieldPqMaxPQ.setAccessible(true);
                    Object pqMaxPQ = fieldPqMaxPQ.get(maxPQ);

                    System.out.print("maxPQ: ");
                    for (Object elem : (Object[]) pqMaxPQ) {
                        if (elem != null) {
                            System.out.print(elem + " ");
                        }
                    }
                    System.out.println();

                    //reflection
                    Class clazzMinPQ = minPQ.getClass();
                    Field fieldPqMinPQ = clazzMinPQ.getDeclaredField("pq");
                    fieldPqMinPQ.setAccessible(true);
                    Object pqMinPQ = fieldPqMinPQ.get(minPQ);

                    System.out.print("minPQ: ");
                    for (Object elem : (Object[]) pqMinPQ) {
                        if (elem != null) {
                            System.out.print(elem + " ");
                        }
                    }
                    System.out.println();
                    System.out.println();
                }

                //result for iteration #
                if (i+1 == 9876) {
                    System.out.println("<--- RESULT FOR ITERATION #" + (i+1) + " --->");
                    if (minPQ.size() > maxPQ.size()) {
                        System.out.println(minPQ.min());
                    } else if (minPQ.size() < maxPQ.size()) {
                        System.out.println(maxPQ.max());
                    } else {
                        System.out.println(maxPQ.max());
                        System.out.println(minPQ.min());
                    }
                }

            }

            System.out.println("<---RESULT--->");
            if (minPQ.size() > maxPQ.size()) {
                System.out.println(minPQ.min());
            } else if (minPQ.size() < maxPQ.size()) {
                System.out.println(maxPQ.max());
            } else {
                System.out.println(maxPQ.max());
                System.out.println(minPQ.min());
            }


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

    }

}
