import java.io.*;
import java.util.Arrays;

/**
 * Created by kaeltas on 19.02.15.
 */
public class Main {

    private int [] readFile(File file) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(file));

        int count = Integer.valueOf(br.readLine());
        int [] readArray = new int [count];
        for (int i = 0; i < count; i++) {
            readArray[i] = Integer.valueOf(br.readLine());
        }

        return readArray;
    }


    public static void main(String[] args) {

        Main testMain = new Main();
        try {
            //int[] A = testMain.readFile(new File("test-data/data_examples_03/input__1000.txt"));
            int[] A = testMain.readFile(new File("test-data/input__10000.txt"));

            int [] cloneA = A.clone();
            System.out.println(new QuickSort().doQuickSort(cloneA, QuickSort.PARTITION_PIVOT_LAST));
            System.out.println(Arrays.toString(cloneA));

            cloneA = A.clone();
            System.out.println(new QuickSort().doQuickSort(cloneA, QuickSort.PARTITION_PIVOT_FIRST));
            System.out.println(Arrays.toString(cloneA));

            cloneA = A.clone();
            System.out.println(new QuickSort().doQuickSort(cloneA, QuickSort.PARTITION_PIVOT_MEDIAN));
            System.out.println(Arrays.toString(cloneA));

        } catch (IOException e) {
            System.out.println("Error while reading file");
        }

    }

}
