import java.util.Arrays;

/**
 * Created by kaeltas on 19.02.15.
 */
public class QuickSort {

    public static Partition PARTITION_PIVOT_LAST = new PartitionPivotLast();
    public static Partition PARTITION_PIVOT_FIRST = new PartitionPivotFirst();
    public static Partition PARTITION_PIVOT_MEDIAN = new PartitionPivotMedian();

    private static class Pair {
        public final int q;
        public final int comparesCount;

        public Pair(int q, int comparesCount) {
            this.q = q;
            this.comparesCount = comparesCount;
        }
    }

    interface Partition {
        public Pair partition(int [] A, int p, int r);
    }

    //Last element used as Pivot
    private static class PartitionPivotLast implements Partition {
        @Override
        public Pair partition(int [] A, int p, int r) {
            int comparesCounter = 0;
            int x = A[r];
            int i = p-1;
            for (int j = p; j < r; j++) {
                comparesCounter++;
                if (A[j] < x) {
                    i++;
                    swap(A, i, j);
                }
            }
            swap(A, i+1, r);
            return new Pair(i+1, comparesCounter);
        }
    }

    //First element used as Pivot
    private static class PartitionPivotFirst implements Partition {
        @Override
        public Pair partition(int [] A, int p, int r) {
            int comparesCounter = 0;
            swap(A, p, r);
            int x = A[r];
            int i = p-1;
            for (int j = p; j < r; j++) {
                comparesCounter++;
                if (A[j] < x) {
                    i++;
                    swap(A, i, j);
                }
            }
            swap(A, i+1, r);
            return new Pair(i+1, comparesCounter);
        }
    }

    //Median between firts, last and middle elements used as Pivot
    private static class PartitionPivotMedian implements Partition {
        @Override
        public Pair partition(int [] A, int p, int r) {
            int comparesCounter = 0;

            //find medianIndex
            int median = A[p];
            int [] sorted3Ar = new int [] {A[p], A[(p+r)/2], A[r]};
            Arrays.sort(sorted3Ar);
            int medianIndex = p;
            if (sorted3Ar[1] == A[r]) {
                medianIndex = r;
            } else if (sorted3Ar[1] == A[(p+r)/2]) {
                medianIndex = (p+r)/2;
            }

            //exchange median element and last element
            swap(A, medianIndex, r);

            int x = A[r];
            int i = p-1;
            for (int j = p; j < r; j++) {
                comparesCounter++;
                if (A[j] < x) {
                    i++;
                    swap(A, i, j);
                }
            }
            swap(A, i+1, r);
            return new Pair(i+1, comparesCounter);
        }
    }

    private int recursiveQuickSort(int A[], int p, int r, Partition part) {
        if (p >= r) {
            return 0;
        }

        Pair pair = part.partition(A, p, r);
        int comparesCountLeft = recursiveQuickSort(A, p, pair.q -1, part);
        int comparesCountRight = recursiveQuickSort(A, pair.q +1, r, part);

        return comparesCountLeft + comparesCountRight + pair.comparesCount;
    }

    /**
     * Uses last element as Pivot in partition method
     * @param A
     * @return number of compares
     */
    public int doQuickSort(int [] A) {
        return recursiveQuickSort(A, 0, A.length-1, PARTITION_PIVOT_LAST);
    }

    /**
     * Uses custom partition element as Pivot in partition method
     * @param A
     * @return number of compares
     */
    public int doQuickSort(int [] A, Partition partition) {
        return recursiveQuickSort(A, 0, A.length-1, partition);
    }

    private static void swap(int [] A, int i, int j) {
        int tmp = A[i];
        A[i] = A[j];
        A[j] = tmp;
    }

    public static void main(String[] args) {
        int [] testA = new int [] {4, 6, 2, 1, 5, 9, 7, 8, 0, 3};
        int [] testB = new int [] {6, 10, 2, 4, 1, 3, 5, 7, 9, 8};

        int compares = new QuickSort().doQuickSort(testB, PARTITION_PIVOT_LAST);

        System.out.println(Arrays.toString(testB));
        System.out.println(compares);
    }





}
