import java.io.*;
import java.util.*;

/**
 * Created by kaeltas on 25.02.15.
 */
public class CountingSort {

    /**
     * Sorts array of strings with equal length().
     * Each string must contain only small english letters [a-z].
     * @param a input array (would be modified)
     * @param d radix(digit) to sort array by
     */
    public static void sort(String [] a, int d) {
        int[] c = new int[(int)'z'+1];
        String[] b = new String[a.length];

        for (int i = 0; i < a.length; i++) {
            c[(int)a[i].charAt(d)]++;
        }

        for (int i = 1; i < c.length; i++) {
            c[i] += c[i-1];
        }

        for (int i = a.length-1; i >= 0; i--) {
            b[c[(int)a[i].charAt(d)]-1] = a[i];
            c[(int)a[i].charAt(d)]--;
        }

        for (int i = 0; i < a.length; i++) {
            a[i] = b[i];
        }
    }

    public static Character [] mostFrequentLetters(String[] a) {
        Map<Character, Integer> map = new HashMap<Character, Integer>();

        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[i].length(); j++) {
                char ch = a[i].charAt(j);
                map.put(ch, map.get(ch) == null ? 1 : map.get(ch) + 1);
            }
        }

        List<Map.Entry<Character, Integer>> list = new LinkedList<Map.Entry<Character, Integer>>(map.entrySet());
        Collections.sort(list, new Comparator<Map.Entry<Character, Integer>>() {
            @Override
            public int compare(Map.Entry<Character, Integer> e1, Map.Entry<Character, Integer> e2) {
                return e2.getValue() - e1.getValue();
            }
        });

        //System.out.println(list);

        List<Character> result = new LinkedList<Character>();
        Map.Entry<Character, Integer> entryPrev = null;
        for (Map.Entry<Character, Integer> entry : list) {
            if (entryPrev == null) {
                entryPrev = entry;
            }

            if (!entryPrev.getValue().equals(entry.getValue())) {
                break;
            }

            result.add(entry.getKey());

            entryPrev = entry;
        }

        return result.toArray(new Character[0]);

    }



    public static void main(String[] args) {

        List<String> list = new ArrayList<String>();

        try {
            BufferedReader br = new BufferedReader(new FileReader(new File("test-data/anagrams.txt")));

            String line;
            while ((line = br.readLine()) != null) {
                list.add(line);
            }

            System.out.println("--Sort array--");
            String [] ar = list.toArray(new String[0]);
            System.out.println("->Sort by third letter--");
            sort(ar, 2); //sort by third letter
            System.out.println("first=" + ar[0]);
            System.out.println("last=" + ar[ar.length-1]);
            System.out.println("->Sort by second letter--");
            sort(ar, 1); //sort by second letter
            System.out.println("first=" + ar[0]);
            System.out.println("last=" + ar[ar.length-1]);
            System.out.println("->Sort by first letter--");
            sort(ar, 0); //sort by first letter
            System.out.println("first=" + ar[0]);
            System.out.println("last=" + ar[ar.length-1]);

            //System.out.println(Arrays.toString(ar));

            System.out.println("--Most frequent letters--");
            System.out.println(Arrays.toString(mostFrequentLetters(ar)));


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
