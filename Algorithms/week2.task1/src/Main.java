import java.io.*;
import java.util.Arrays;

/**
 * Created by kaeltas on 14.02.15.
 */
public class Main {

    private int countUsers;
    private int countFilms;
    private int d [] []; //preference matrix

    public void readPrefMatrixFromFile(File file) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
        String line;
        line = bufferedReader.readLine();
        String [] params = line.split(" ");
        countUsers = Integer.valueOf(params[0]);
        countFilms = Integer.valueOf(params[1]);

        d = new int [countUsers][countFilms];
        for (int i = 0; i < countUsers; i++) {
            line = bufferedReader.readLine();
            String [] films = line.split(" ");
            for (int j = 0; j < countFilms; j++) {
                d[i][j] = Integer.valueOf(films[j + 1]); //skip user sequence number => j+1
            }
        }
    }

    //find inversions between users: k, l
    //counting from 1 till countUsers included
    public int findInversionsBetweenUsers(int k, int l) {
        int [] a = new int [countFilms];
        for (int i = 0; i < countFilms; i++) {
            a[d[k-1][i]-1] = d[l-1][i];
        }

        return Inversions.sortAndCountInv(a);
    }

    public static void main(String[] args) throws IOException {

        Main main = new Main();
        main.readPrefMatrixFromFile(new File("test/input_1000_100.txt"));
        System.out.println(main.findInversionsBetweenUsers(951, 178));


    }

}
