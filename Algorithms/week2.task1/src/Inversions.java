import java.util.Arrays;

/**
 * Created by kaeltas on 14.02.15.
 */
public class Inversions {
    static private class Pair {
        int [] A;
        int count;

        public Pair(int[] a, int count) {
            A = a;
            this.count = count;
        }
    }

    static public int sortAndCountInv(int [] a) {
        int [] aAux = new int [a.length];
        Pair pair = sortAndCountInv(a, aAux, 0, a.length);
        return pair.count;
    }

    static private Pair sortAndCountInv(int [] a, int [] aAux, int l, int r) {

        if (l >= r-1) {
            return new Pair(a, 0);
        }

        int mid = (l+r)/2;
        Pair pairL = sortAndCountInv(a, aAux, l, mid);
        Pair pairR = sortAndCountInv(a, aAux, mid, r);
        Pair pairZ = mergeAndCountInv(a, aAux, l, mid, r);

        return new Pair(a, pairL.count+pairR.count+pairZ.count);
    }

    static private Pair mergeAndCountInv(int[] a, int [] aAux, int l, int mid, int r) {

        for (int i = l; i < r; i++) {
            aAux[i] = a[i];
        }

        int countInv = 0;
        int i = l;
        int j = mid;
        for (int k = l; k < r; k++) {
            if (j >= r) {
                a[k] = aAux[i++];
            } else if (i >= mid) {
                a[k] = aAux[j++];
            } else if (aAux[i] < aAux[j]) {
                a[k] = aAux[i++];
            } else /*if (aAux[i] >= aAux[j])*/ {
                a[k] = aAux[j++];
                countInv += mid-i;
            }

        }

        return new Pair(a, countInv);
    }

    public static void main(String[] args) {

        //Test
        int [] a = {5, 3, 1, 4, 2}; //count inversions 4+2+1=7
        System.out.println(Inversions.sortAndCountInv(a));
        System.out.println(Arrays.toString(a));

    }
}
