import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * Created by kaeltas on 06.03.15.
 */
public class SumFinder implements Runnable {

    private Set<Long> set;
    private Integer sumCounter;

    public SumFinder(Set<Long> set) {
        this.set = set;
        sumCounter = 0;
    }

    @Override
    public void run() {
        List<Long> list = new LinkedList<Long>();
        for (long i = -1000; i <= 1000 ; i++) {
            list.add(i);
        }

        Integer c = set.size();
        for (Long l : set) {
            if ( c-- % 1000 == 0) {
                System.out.println("Left: " + c/1000 + "k");
            }

            Iterator<Long> iter = list.iterator();
            while (iter.hasNext()) {
                Long s = iter.next();
                if (set.contains(s-l)) {
                    System.out.println(s);
                    sumCounter++;
                    iter.remove();
                }
            }

        }
    }

    public Integer getSumCounter() {
        return sumCounter;
    }
}
