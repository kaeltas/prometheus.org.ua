import java.io.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by kaeltas on 06.03.15.
 */
public class Main {

    public static void main(String[] args) {

        try {
            BufferedReader br = new BufferedReader(new FileReader(new File("test-data/input_06.txt")));

            Set<Long> set = new HashSet<Long>();

            String line;
            while ((line = br.readLine()) != null) {
                set.add(Long.valueOf(line));
            }

            System.out.println("File-read complete");
            System.out.println("set.size() = " + set.size());

            Long startMillis = System.currentTimeMillis();

            SumFinder sumFinder = new SumFinder(set);
            Thread th = new Thread(sumFinder);
            th.start();
            th.join();

            System.out.println("working time: " + (System.currentTimeMillis()-startMillis)/1000);

            System.out.println(sumFinder.getSumCounter());

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

}
